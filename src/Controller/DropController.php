<?php



namespace App\Controller;


use Aws\S3\S3Client;
use Aws\S3\ObjectUploader;
use transloadit\Transloadit;
use Aws\S3\MultipartUploader;
use Aws\Exception\AwsException;
use Aws\MediaConvert\MediaConvertClient;
use Aws\Exception\MultipartUploadException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DropController extends AbstractController
{
    /**
     * @Route("/drop", name="drop")
     */
    public function index(): Response
    {
        
        return $this->render('drop/index.html.twig', [
            'controller_name' => 'DropController',
        ]);
    }


    /**
     * @Route("/dropvideo", name="dropvideo")
     */
    public function DropVideo(Request $requestVideo)
    {

      // RECUPERATION DU NOM DU FICHIER

      $nameVideo = $requestVideo->request->get('inputVideo');

      // ENVOIE DU FICHIER DANS S3

      $client = new S3Client([
        'version' => 'latest',
        'region'  => 'eu-west-3',
        'scheme' => 'http',
        'credentials' => [
            'key'    => 'AKIAZSBDROWOCA7ESNW6',
            'secret' => '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'
        ]
      ]);

      $fichier = $nameVideo;
  
      $bucket = 'awelty-bucket';
      $file_Path = __DIR__ . '/../../'.$fichier.'';
      $key = basename($file_Path);

      $source = fopen($file_Path, 'rb');

      $uploader = new ObjectUploader(
        $client,
        $bucket,
        $key,
        $source,
        'public-read',
      );

      do {
        try {
            $result = $uploader->upload();
            if ($result["@metadata"]["statusCode"] == '200') {
                    print('<p>File successfully uploaded to ' . $result["ObjectURL"] . '.</p>');
            }
        } catch (MultipartUploadException $e) {
            rewind($source);
            $uploader = new MultipartUploader($client, $source, [
                'state' => $e->getState(),
                'acl' => 'public-read',
            ]);
        }
    } while (!isset($result));
    
    fclose($source);
    

    // CONVERSION DE LA VIDEO MP3 EN HLS

    $mediaConvertClient = new MediaConvertClient([
      'version' => 'latest',
      'region' => 'eu-west-3',
      'endpoint' => 'https://187dshywc.mediaconvert.eu-west-3.amazonaws.com',
      'credentials' => [
          'key'    => 'AKIAZSBDROWOCA7ESNW6',
          'secret' => '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',
      ],
      'http'    => [
          'verify' => 'C:\wamp64\www\MonProjet\aws2\cacert.pem'
      ]
  ]);
  $jobSetting = [
      "OutputGroups" => [
          [
              "Name" => "Apple HLS",
              "OutputGroupSettings" => [
                  "Type" => "HLS_GROUP_SETTINGS",
                  "HlsGroupSettings"=> [
                      "SegmentLength"=> 10,
                      "Destination"=> "s3://awelty-bucket/",
                      "MinSegmentLength"=> 0
                  ]
              ],
              "Outputs" => [
                  [
                      // Will use default Audio Source 1
                      "Preset" => "System-Avc_16x9_360p_29_97fps_600kbps",
                      "NameModifier" => "_watermarked3"
                  ],
              ]
          ]
      ],
      "AdAvailOffset" => 0,
      "Inputs" => [
          [
              "AudioSelectors" => [
                  "Audio Selector 1" => [
                      "Offset" => 0,
                      "DefaultSelection" => "DEFAULT",
                      "ProgramSelection" => 1,
                      "SelectorType" => "TRACK",
                      "Tracks" => [
                          1
                      ]
                  ],
              ],
              "VideoSelector" => [
                  "ColorSpace" => "FOLLOW"
              ],
              "FilterEnable" => "AUTO",
              "PsiControl" => "USE_PSI",
              "FilterStrength" => 0,
              "DeblockFilter" => "DISABLED",
              "DenoiseFilter" => "DISABLED",
              "TimecodeSource" => "ZEROBASED",
              "ImageInserter"=> [
                  "InsertableImages"=> [
                    [
                      "Width"=> 800,
                      "Height"=> 800,
                      "ImageX"=> 50,
                      "ImageY"=> 50,
                      "Duration"=> 30000,
                      "FadeIn"=> 2000,
                      "Layer"=> 1,
                      "ImageInserterInput"=> "s3://awelty-bucket/image.png",
                      "StartTime"=> "00:00:05;01",
                      "Opacity"=> 80
                    ]
                  ]
                    ],
              "FileInput" => "s3://awelty-bucket/$fichier"
          ]
      ],
      "TimecodeConfig" => [
          "Source" => "EMBEDDED"
      ]
  ];
  try {
      $result = $mediaConvertClient->createJob([
          "Role" => "arn:aws:iam::657204540828:role/service-role/MediaConvert_Default_Role",
          "Settings" => $jobSetting, //JobSettings structure
          "Queue" => "arn:aws:mediaconvert:eu-west-3:657204540828:queues/Default",
          "UserMetadata" => [
              "Customer" => "Amazon"
          ],
      ]);
  } catch (AwsException $e) {
      // output error message if fails
      echo $e->getMessage();
      echo "\n";
  } 




      return $this->render('drop/video.html.twig',[
        'name' => $nameVideo,
      ]);
    }

    

      /**
     * @Route("/dropson", name="dropson")
     */
      public function DropSon(Request $requestSong)
      {
          // RECUPERATION DU NOM DU FICHIER

          $nameSong = $requestSong->request->get('inputSon');

          // ENVOIE DU FICHIER DANS S3

          $client = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-west-3',
            'scheme' => 'http',
            'credentials' => [
                'key'    => 'AKIAZSBDROWOCA7ESNW6',
                'secret' => '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'
            ]
          ]);

          $fichier = $nameSong;
      
          $bucket = 'awelty-bucket';
          $file_Path = __DIR__ . '/../../'.$fichier.'';
          $key = basename($file_Path);

          $source = fopen($file_Path, 'rb');

          $uploader = new ObjectUploader(
            $client,
            $bucket,
            $key,
            $source,
            'public-read',
          );

          do {
            try {
                $result = $uploader->upload();
                if ($result["@metadata"]["statusCode"] == '200') {
                        print('<p>File successfully uploaded to ' . $result["ObjectURL"] . '.</p>');
                }
            } catch (MultipartUploadException $e) {
                rewind($source);
                $uploader = new MultipartUploader($client, $source, [
                    'state' => $e->getState(),
                    'acl' => 'public-read',
                ]);
            }
        } while (!isset($result));
        
        fclose($source);

        sleep(1);

        // TRANSFORMATION DU MP3 STOCKE DANS S3

        $transloadit = new Transloadit([
            "key" => "786d955c83f745a7a82e0c4f3a2ed74d",
            "secret" => "d17bdec1d1afba502a6ca7026561702ef355e30a",
          ]);
          
          // Start the Assembly
          $response = $transloadit->createAssembly([
            "params" => [
              "template_id" => "8cfce6b9ba074889aa0ac00eaa7e8d2e",
              "fields" => [
                "name" => $nameSong,
              ]
            ]

          ]);

          return $this->render('drop/music.html.twig',[
            'response' => $response,
            'name' => $nameSong,
          ]);
        }



        

        /**
         * @Route("/dropimage", name="dropimage")
         */
        public function DropImage(Request $request)
        {

          // RECUPERATION DU NOM DU FICHIER

          $name = $request->request->get('inputPhoto');
          
          // ENVOIE DU FICHIER DANS S3

          $client = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-west-3',
            'scheme' => 'http',
            'credentials' => [
                'key'    => 'AKIAZSBDROWOCA7ESNW6',
                'secret' => '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'
            ]
          ]);

          $fichier = $name;
      
          $bucket = 'awelty-bucket';
          $file_Path = __DIR__ . '/../../'.$fichier.'';
          $key = basename($file_Path);

          $source = fopen($file_Path, 'rb');

          $uploader = new ObjectUploader(
            $client,
            $bucket,
            $key,
            $source,
            'public-read',
          );

          do {
            try {
                $result = $uploader->upload();
                if ($result["@metadata"]["statusCode"] == '200') {
                        print('<p>File successfully uploaded to ' . $result["ObjectURL"] . '.</p>');
                }
            } catch (MultipartUploadException $e) {
                rewind($source);
                $uploader = new MultipartUploader($client, $source, [
                    'state' => $e->getState(),
                    'acl' => 'public-read',
                ]);
            }
        } while (!isset($result));
        
        fclose($source);

          // AJOUT DE LA WATERMARK A L'IMAGE STOCKE DANS S3

          $transloadit = new Transloadit([
              "key" => "786d955c83f745a7a82e0c4f3a2ed74d",
              "secret" => "d17bdec1d1afba502a6ca7026561702ef355e30a",
            ]);
            
            // Start the Assembly
            $response = $transloadit->createAssembly([
              "params" => [
                "template_id" => "92546fd47f944f59a098f8cab1610c15",
                "fields" => [
                  "name" => $name,
                ]
              ]

            ]);

            return $this->render('drop/image.html.twig',[
              'response' => $response,
            ]);
    }


}
