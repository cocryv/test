<?php


namespace App\Controller;

use Aws\S3\S3Client;
use Aws\MediaConvert;
use Aws\S3\ObjectUploader;
use transloadit\Transloadit;
use Aws\S3\MultipartUploader;
use Aws\Exception\AwsException;
use Illuminate\Http\UploadedFile;
use Aws\CloudFront\CloudFrontClient;
use Aws\MediaConvert\MediaConvertClient;
use Doctrine\ORM\EntityManagerInterface;
use Aws\Exception\MultipartUploadException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;


class ApiController extends AbstractController
{

    /**
     * @Route("/api/image", name="api_image",methods={"POST"})
     */
    public function addImage(Request $request){

        $file = $request->files->get('image');
        $fileName = (md5(uniqid())).'.'.$file->guessExtension();

        $client = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-west-3',
            'scheme' => 'http',
            'credentials' => [
                'key'    => 'AKIAZSBDROWOCA7ESNW6',
                'secret' => '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'
            ]
          ]);
      
          $bucket = 'awelty-bucket';
          $key = 'images/' . basename($fileName);

          $source = fopen($file, 'rb');

          $uploader = new ObjectUploader(
            $client,
            $bucket,
            $key,
            $source,
            'public-read',
          );

          do {
            try {
                $result = $uploader->upload();
            } catch (MultipartUploadException $e) {
                rewind($source);
                $uploader = new MultipartUploader($client, $source, [
                    'state' => $e->getState(),
                    'acl' => 'public-read',
                ]);
            }
        } while (!isset($result));
        
        fclose($source);


        // AJOUT DE LA WATERMARK A L'IMAGE STOCKE DANS S3

        $transloadit = new Transloadit([
            "key" => "786d955c83f745a7a82e0c4f3a2ed74d",
            "secret" => "d17bdec1d1afba502a6ca7026561702ef355e30a",
          ]);
          
          // Start the Assembly
          $response = $transloadit->createAssembly([
            "params" => [
              "template_id" => "92546fd47f944f59a098f8cab1610c15",
              "fields" => [
                "name" => $fileName,
              ]
            ]
          ]);


        $cloudFrontClient = new CloudFrontClient([
            'profile' => 'default',
            'version' => '2014-11-06',
            'region' => 'us-west-2'
        ]);

        try {
            $result = $cloudFrontClient->getSignedUrl([
                'url' => "https://d352zaypcczwzh.cloudfront.net/images/$fileName",
                'expires' => time() + 300,
                'private_key' => dirname(__DIR__) . '/private_key.pem',
                'key_pair_id' => "K6AMCK04QBX89"
            ]);
        
        } catch (AwsException $e) {
            return 'Error: ' . $e->getAwsErrorMessage();
        }

        $response = new Response();
        $response->setContent($result);
        $response->headers->set('Content-Type', 'application/json');



        return $response;

}


    /**
    * @Route("/api/son", name="api_son",methods={"POST"})
    */
    public function addSon(Request $request)
    {
        $file = $request->files->get('son');
        $fileName = (md5(uniqid())).'.'.$file->guessExtension();

        $client = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-west-3',
            'scheme' => 'http',
            'credentials' => [
                'key'    => 'AKIAZSBDROWOCA7ESNW6',
                'secret' => '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'
            ]
          ]);
      
          $bucket = 'awelty-bucket';
          $key = 'son/' . basename($fileName);

          $source = fopen($file, 'rb');

          $uploader = new ObjectUploader(
            $client,
            $bucket,
            $key,
            $source,
            'public-read',
          );

          do {
            try {
                $result = $uploader->upload();
                if ($result["@metadata"]["statusCode"] == '200') {
                        print('<p>File successfully uploaded to ' . $result["ObjectURL"] . '.</p>');
                }
            } catch (MultipartUploadException $e) {
                rewind($source);
                $uploader = new MultipartUploader($client, $source, [
                    'state' => $e->getState(),
                    'acl' => 'public-read',
                ]);
            }
        } while (!isset($result));
        
        fclose($source);

        sleep(1);

        // TRANSFORMATION DU MP3 STOCKE DANS S3

        $transloadit = new Transloadit([
            "key" => "786d955c83f745a7a82e0c4f3a2ed74d",
            "secret" => "d17bdec1d1afba502a6ca7026561702ef355e30a",
        ]);
          
        // Start the Assembly
        $response = $transloadit->createAssembly([
        "params" => [
          "template_id" => "8cfce6b9ba074889aa0ac00eaa7e8d2e",
          "fields" => [
            "name" => $fileName,
            ]
        ]]);

        sleep(5);

        $mediaConvertClient = new MediaConvertClient([
            'version' => 'latest',
            'region' => 'eu-west-3',
            'endpoint' => 'https://187dshywc.mediaconvert.eu-west-3.amazonaws.com',
            'credentials' => [
                'key'    => 'AKIAZSBDROWOCA7ESNW6',
                'secret' => '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',
            ],
            'http'    => [
                'verify' => 'C:\wamp64\www\MonProjet\aws2\cacert.pem'
            ]
        ]);
        $jobSetting = [
            "OutputGroups" => [
                [
                    "Name" => "Apple HLS",
                    "OutputGroupSettings" => [
                        "Type" => "HLS_GROUP_SETTINGS",
                        "HlsGroupSettings"=> [
                            "SegmentLength"=> 10,
                            "Destination"=> "s3://awelty-bucket/son/",
                            "MinSegmentLength"=> 0
                        ]
                    ],
                    "Outputs" => [
                        [
                            // Will use default Audio Source 1
                            "ContainerSettings" => [
                                "Container"=> "M3U8",
                                "M3u8Settings"=> []
                            ],
                            "AudioDescriptions"=>[
                                [
                                    "AudioSourceName"=> "Audio Selector 1",
                                    "CodecSettings"=>[
                                        "Codec"=> "AAC",
                                        "AacSettings"=>[
                                            "Bitrate"=>96000,
                                            "CodingMode"=> "CODING_MODE_2_0",
                                            "SampleRate"=> 48000
                                        ]
                                    ]
                                ]
                            ],
                            "OutputSettings"=>[
                                "HlsSettings"=> []
                            ],
                            "NameModifier"=> "_streaming"
                        ],
                    ]
                ]
            ],
            "AdAvailOffset" => 0,
            "Inputs" => [
                [
                    "AudioSelectors" => [
                        "Audio Selector 1" => [
                            "Offset" => 0,
                            "DefaultSelection" => "DEFAULT",
                            "ProgramSelection" => 1,
                            "SelectorType" => "TRACK",
                            "Tracks" => [
                                1
                            ]
                        ],
                    ],
                    "FilterEnable" => "AUTO",
                    "PsiControl" => "USE_PSI",
                    "FilterStrength" => 0,
                    "DeblockFilter" => "DISABLED",
                    "DenoiseFilter" => "DISABLED",
                    "TimecodeSource" => "ZEROBASED",
                    "FileInput" => "s3://awelty-bucket/son/watermarked_3ffbcf3a57abe3509b5795dacb9cc992.mp3"
                ]
            ],
            "TimecodeConfig" => [
                "Source" => "EMBEDDED"
            ]
        ];
        try {
            $result = $mediaConvertClient->createJob([
                "Role" => "arn:aws:iam::657204540828:role/service-role/MediaConvert_Default_Role",
                "Settings" => $jobSetting, //JobSettings structure
                "Queue" => "arn:aws:mediaconvert:eu-west-3:657204540828:queues/Default",
                "UserMetadata" => [
                    "Customer" => "Amazon"
                ],
            ]);
        } catch (AwsException $e) {
            // output error message if fails
            echo $e->getMessage();
            echo "\n";
        } 


        // $cloudFrontClient = new CloudFrontClient([
        //     'profile' => 'default',
        //     'version' => '2014-11-06',
        //     'region' => 'us-west-2'
        // ]);

        // try {

        //     $bName = basename($fileName,".mp3");
        //     $result = $cloudFrontClient->getSignedUrl([
        //         'url' => "https://d352zaypcczwzh.cloudfront.net/son/watermarked_$bName.m3u8",
        //         'expires' => time() + 300,
        //         'private_key' => dirname(__DIR__) . '/private_key.pem',
        //         'key_pair_id' => "K6AMCK04QBX89"
        //     ]);
        
        // } catch (AwsException $e) {
        //     return 'Error: ' . $e->getAwsErrorMessage();
        // }

        $test = print_r($result);
        $response = new Response();
        $response->setContent($test);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


    /**
    * @Route("/api/video", name="api_video",methods={"POST"})
    */
    public function addVideo(Request $request)
    {
        $file = $request->files->get('video');
        $fileName = (md5(uniqid())).'.'.$file->guessExtension();

        $client = new S3Client([
            'version' => 'latest',
            'region'  => 'eu-west-3',
            'scheme' => 'http',
            'credentials' => [
                'key'    => 'AKIAZSBDROWOCA7ESNW6',
                'secret' => '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'
            ]
          ]);
      
          $bucket = 'awelty-bucket';
          $key = 'video/' . basename($fileName);

          $source = fopen($file, 'rb');

          $uploader = new ObjectUploader(
            $client,
            $bucket,
            $key,
            $source,
            'public-read',
          );

          do {
            try {
                $result = $uploader->upload();
                if ($result["@metadata"]["statusCode"] == '200') {
                        print('<p>File successfully uploaded to ' . $result["ObjectURL"] . '.</p>');
                }
            } catch (MultipartUploadException $e) {
                rewind($source);
                $uploader = new MultipartUploader($client, $source, [
                    'state' => $e->getState(),
                    'acl' => 'public-read',
                ]);
            }
        } while (!isset($result));
        
        fclose($source);

        // CONVERSION DE LA VIDEO MP4 EN HLS

        $mediaConvertClient = new MediaConvertClient([
            'version' => 'latest',
            'region' => 'eu-west-3',
            'endpoint' => 'https://187dshywc.mediaconvert.eu-west-3.amazonaws.com',
            'credentials' => [
                'key'    => 'AKIAZSBDROWOCA7ESNW6',
                'secret' => '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',
            ],
            'http'    => [
                'verify' => 'C:\wamp64\www\MonProjet\aws2\cacert.pem'
            ]
        ]);
        $jobSetting = [
            "OutputGroups" => [
                [
                    "Name" => "Apple HLS",
                    "OutputGroupSettings" => [
                        "Type" => "HLS_GROUP_SETTINGS",
                        "HlsGroupSettings"=> [
                            "SegmentLength"=> 10,
                            "Destination"=> "s3://awelty-bucket/video/",
                            "MinSegmentLength"=> 0
                        ]
                    ],
                    "Outputs" => [
                        [
                            // Will use default Audio Source 1
                            "Preset" => "System-Avc_16x9_360p_29_97fps_600kbps",
                            "NameModifier" => "_watermarked3"
                        ],
                    ]
                ]
            ],
            "AdAvailOffset" => 0,
            "Inputs" => [
                [
                    "AudioSelectors" => [
                        "Audio Selector 1" => [
                            "Offset" => 0,
                            "DefaultSelection" => "DEFAULT",
                            "ProgramSelection" => 1,
                            "SelectorType" => "TRACK",
                            "Tracks" => [
                                1
                            ]
                        ],
                    ],
                    "VideoSelector" => [
                        "ColorSpace" => "FOLLOW"
                    ],
                    "FilterEnable" => "AUTO",
                    "PsiControl" => "USE_PSI",
                    "FilterStrength" => 0,
                    "DeblockFilter" => "DISABLED",
                    "DenoiseFilter" => "DISABLED",
                    "TimecodeSource" => "ZEROBASED",
                    "ImageInserter"=> [
                        "InsertableImages"=> [
                        [
                            "Width"=> 800,
                            "Height"=> 800,
                            "ImageX"=> 50,
                            "ImageY"=> 50,
                            "Duration"=> 30000,
                            "FadeIn"=> 2000,
                            "Layer"=> 1,
                            "ImageInserterInput"=> "s3://awelty-bucket/image.png",
                            "StartTime"=> "00:00:05;01",
                            "Opacity"=> 80
                        ]
                        ]
                        ],
                    "FileInput" => "s3://awelty-bucket/video/$fileName"
                ]
            ],
            "TimecodeConfig" => [
                "Source" => "EMBEDDED"
            ]
        ];
        try {
            $result = $mediaConvertClient->createJob([
                "Role" => "arn:aws:iam::657204540828:role/service-role/MediaConvert_Default_Role",
                "Settings" => $jobSetting, //JobSettings structure
                "Queue" => "arn:aws:mediaconvert:eu-west-3:657204540828:queues/Default",
                "UserMetadata" => [
                    "Customer" => "Amazon"
                ],
            ]);
        } catch (AwsException $e) {
            // output error message if fails
            echo $e->getMessage();
            echo "\n";
        } 
        $cloudFrontClient = new CloudFrontClient([
            'profile' => 'default',
            'version' => '2014-11-06',
            'region' => 'us-west-2'
        ]);

        try {
            $result = $cloudFrontClient->getSignedUrl([
                'url' => "https://d352zaypcczwzh.cloudfront.net/video/$fileName",
                'expires' => time() + 300,
                'private_key' => dirname(__DIR__) . '/private_key.pem',
                'key_pair_id' => "K6AMCK04QBX89"
            ]);
        
        } catch (AwsException $e) {
            return 'Error: ' . $e->getAwsErrorMessage();
        }

        $response = new Response();
        $response->setContent($result);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}



